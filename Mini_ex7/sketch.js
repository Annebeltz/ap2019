//Amalie & Annebel  - mini_ex7

//Variables
let grid_space = 10;
let grid =[];
let cols, rows;
let xPos, yPos;
let dir;
const dotUP = 5;
const dotRIGHT = 10;
const dotDOWN = 10;
const dotLEFT = 3;
let orange;
let purple;
let text;
let song;

//song
function preload(){
  song = loadSound('sang.mp3');
}

function setup() {
  createCanvas(1500,700);
song.play();

//text "hello world"
text = createDiv('HELLO WORLD');
text.position(400, 100);
text.style('font-size', '200px');

//color change + create grid and start position
  orange = color(255, 199, 0);
  purple = color(167, 136, 199);
  background(0, 159, 96);
  grid = drawGrid();
  xPos = floor(cols/2);
  yPos = floor(rows/-2);
  dir = dotUP;
}

function draw() {

//speed
  for (let n = 0; n < 10; n++) {
  checkEdges();
  let state = grid[xPos][yPos];

  //check state
//rule 1
  if (state == 1) {
    dir++;  // turn right
    grid[xPos][yPos] = 0; //change
    fill(167, 136, 199);

//rule 2
    if (dir > dotLEFT) {
      dir = dotUP;
    }
  }else{
    dir--;  //turn left
    grid[xPos][yPos] = 1;
    fill(orange);

// rule 3
    if (dir < dotUP) {
      dir = dotLEFT;
    }
  }
  ellipse(xPos*grid_space, yPos*grid_space, grid_space, grid_space);
  nextMove();
  }
}

//
function drawGrid() {
  cols = width/grid_space;
  rows = height/grid_space;
  let arr = new Array(cols);
  for (let i=0; i < cols; i++) {//number of cols
    arr[i] = new Array(rows); //array
    for (let j=0; j < rows; j++){ //no of rows
      let x = i * grid_space; //actual x coordinate
      let y = j * grid_space; //actual y coordinate
      noStroke();
      noFill();
      ellipse(x, y, grid_space, grid_space);
      arr[i][j] = 0;  // assign each cell with the status 0 / off
    }
  }
  return arr; //return value
}

//check which direction to go next and set the new current direction
function nextMove () {
  if (dir == dotUP) {
    yPos--;
  } else if (dir == dotRIGHT) {
    xPos++;
  } else if (dir == dotDOWN) {
    yPos++;
  } else if (dir == dotLEFT) {
    xPos--;
  }
}

function checkEdges() {

  if (xPos > cols-1) {
    xPos = 0;
  } else if (xPos < 0) {
    xPos = cols-1;
  }
  if (yPos > rows-1) {
    yPos = 0;
  } else if (yPos < 0) {
    yPos = rows-1;
  }
}
