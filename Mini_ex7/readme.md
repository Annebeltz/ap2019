**Mini_ex7**

**Link:** https://glcdn.githack.com/Annebeltz/ap2019/raw/master/Mini_ex7/index.html

![Screeenshot](Skærmbillede_1.png)

![Screeenshot](Skærmbillede _2.png)


Amalie and I made this program in cooperation. We decided to make a generative program with a spice of randomness, which is visible in our code and rules. Our program consists of three rules. Rule number one is when the yellow dot in our program starts in the bottom of our canvas (y position windowHeight), the dot starts moving from the right side of the canvas to the left. When it reaches the end of the canvas (x position -1) it automatically continues on the right side of the canvas. Rule number two gets activated when the yellow dot strikes its own axis the yellow dot turns purple and rotate 90 degrees to the right, changes its color from purple to yellow and continues the axis above the first one. This is repeated until the dot reaches the top of the canvas (y position -1 ) and then rule number three is activated. The yellow dot turns purple and rotates minus 90 degrees and starts filling out the yellow space in the opposite direction. This pattern continues and the rules get looped each time the dot reaches to rule number three. In this way, there is no end product and it just loops over and over again. This is a deliberate choice that we made in order to get the feeling of infinity and the fact that You can look at it for hours. The rules in our program give it some extent of randomness but clearly more control than generativity.

Furthermore, we added an electric sound and the text "hello world" as You can see in the above screenshots. I am not sure if You are able to hear the music. It worked when we started the atom-live-server but not when using the final link. 

The difference between generative art and automatism is the amount of randomness put into the program or artwork. Generative art is something that employs randomness, where automatism is different. The interesting aspect of generative art is whether the computer or the artist is responsible for the artwork. Who is the author? It is the computer considered as a co-author? In my opinion, the computer is to some extent the author but the artist making the program is the overall artist in this case. Like any other kind of creative art, the artist has some kind of material or tools to work with, like a pencil, a canvas, acrylic colors etc. I think that programming is seen as a tool as well, but when making generative art, the computer makes some of the art too. "software as a material" (Marius Watz 2007).  The artist creates the idea and some rules to constrict the amount of randomness and the computer assists to create the random aspects. 
