# AP2019

**MINI_EX1**

![ScreenShot](Skærmbillede 2019-02-06 kl. 17.07.04.png)

Link:
[runmee](https://cdn.staticaly.com/gl/Annebeltz/ap2019/raw/master/Mini_ex1/index.html)

***Describe your first independent coding process (in relation to thinking, reading, copying, modifying, writing, uploading, sharing, commenting code)***

I have used the folowing syntaxes/codes:

	- cylinder(150, 250, 5, 5);
	
	- fill(355, 504, 0, 10);
	
	- rotateX(frameCount * 0.008);
	
	- rotateY(frameCount * 0.008);

    - background('#fae');
    
At first, my independent coding process was very frustrating. I did not know how to start or what to do. But after the introduction in the class, I started to get a broader view on programming. After the class, I used the p5.js website to start coding. At first, I made an ellipse or just a circle in fact. When I started to play with the different tools and codes I made a 3D cylinder and a background on my canvas. Then I tried to make the cylinder move by using the syntax to make it wider, higher ect. I filled it with color made it go even faster in moving around. I was a bit nervous at the beginning in the class, but when I got home and wasn't distracted by anyone else I had a lot of fun with it. It is still a bit confusing and I haven't tried out the big and complex syntaxes and codes, but I am quite confident in trying out the unknown. 


***How your coding process is differ or similar to reading and writing text? (You may also reflect upon Annette Vee's text on coding literacy)***

I order for the computer to understand your coding you have to be precise. This is not the case in reading and writing. If your gramma is slightly incorrect the person reading it is still able to understand the text.


***What is code and coding/programming practice means to you?***

The practice of programming and coding is very important to me. Learning by doing is a concept that i will notify in aesthetic programming. 
