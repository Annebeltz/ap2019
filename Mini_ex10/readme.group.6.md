**Mini_Ex10 - group 6**

**Global warming idea**

![Screenshot](Skærmbillede.grp6.1.png)


**E-lite idea**

![Screenshot](Skærmbillede.grp6.2.png)





Get together as a study group and brainstorm the forthcoming program for the final project
In the readme file:

**1. Present two different ideas with two different flow charts:**
We have chosen two ideas for our forthcoming program for the final project. Our first idea is an E-lit and our second idea is a throbber concerning global warming.  

**E-literature:**

Our first idea is an e-literature concerning gender and sexuality. We have chosen this topic, as we like E-literature in general and like when a program has a political statement. Thus we have chosen to make a new version of a traditional fairytale, where we question the gender of the prince/ princess/ man/ woman, etc. Our idea is to incorporate the code itself into the statement. Down below you can see a simplification of our program in a flowchart:   


**Global Warming:**

Our second idea concerns global warming. We have chosen to represent global warming by making a throbber circling around an ellipse looking the earth. For each time the throbber reaches the top of the earth, the mainland should become smaller. In the end, all the water/ blue should have overflood the world. Below our throbber and earth, we want to implement an API of Nasa climate change, whereas there should be a live-temperature showing. We have made the simplification of our idea in a flowchart down below: 

**2. What might be the possible technical challenges for the two ideas and how are you going to solve them?**

**E-litterature**

We have thought about some difficulties our program could have: It can be a difficulty if the words won’t change when the canvas changes from old school to futuristic. Technically the JSON format and its implementation might be a difficulty to overcome and to correctly translate and use the data from the JSON file to our program. 
We do not have a certain possible solution to our difficulties, but we will try to look it up in the p5 library and the belonging Daniel Shiffman videos. Furthermore, we are going to use our knowledge about JSON and random syntax as well as generative art. 
Otherwise, we argue that this program won’t be as technically difficult as the global warming idea.


**Global Warming**

Our main concern is the technicality of using an API in our program. It can be difficult to make it work and getting access to the data that we need to make the program how we want. We don’t know if it will be a problem, thus we don’t have a possible solution at the moment, as we will try to make it work along the way. We have talked about (if it becomes a problem) to search for solutions on platforms such as Youtube, GitHub, and other forums.
A technical difficulty might be to “drown” the world in water. Our idea is that the world will be covered partially in water for each time the throbber has been around the world. We thought about one possible solution: Maybe we can use the function millis() and combine it with the operator modulo (%). In this way, we can track how many milliseconds the program has run, and by using modulo you can control that f.ex. for every 1000 milliseconds, a blue ellipse would be drawn upon the earth, making it look like a flood. 
Another maybe easier solution to that issue might be to make the background black and then have moving objects in blue (the water), that becomes bigger per round. The objects should be close to the world and thereby end up covering the world in blue water. 
The last difficulties could be to make the earth look like earth, but our plan is to make many ellipses and place them in such way, that they could look like “mainland” surrounded by water. 

**3. How is this flow chart different from the one that you had in #3 (in terms of the role of a flow chart)?**

We had a discussion in our group regarding the differences between our individual flowcharts and group flowcharts. Although there were some deviations we all had some similarities. The main difference in #3 compared to our group flowcharts is when constructing a flowchart from an already existing program it has some theorized elements because it isn't finished yet. Additionally, it also contains more detail and technical aspects, so we can use the flowchart as a blueprint to finalizing our program. On the contrary, when constructing a flowchart from an idealized idea it becomes rather simple and superficial because its made from an already established idea and no new concepts can be added. 

**4. If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would you reflect upon the notion of algorithms? **

*“Engineers and construction foremen must be able to draw and read blueprints. Programmers must be able to draw and read flowcharts. Flowcharting is a necessary and basic skill for all programmers.” (Ensmenger, 2016, p.331). *

Flowcharts have been a fundamental communication aspect in software. It was established in the 1940s where they had to construct algorithms in the shape of flowcharts to solve a given problem. Afterwards, the programmer had the task to translate the flowchart into “machine language” which the computer was able to understand. The conventionally use of flowcharts is to break down the steps/tasks in a system. Nowadays flowcharts are used in a wider context in terms of both simplifying, communicating and visualizing ideas as well as planning and organizing in a company abroad different professions and groups. It enables us to “see” software on another level.

John von Neumann and Herman Goldstine argue that flowchart is a conceptual technology on a high level, which primary was intended to researchers and other problem specialists to develop algorithmic solutions. 

The order of which things occur is fundamental since both a flowchart and algorithm perform an action or provide coherent information unless certain conditions are met. The concept of chronological thinking is a general aspect that intersects between many different areas and professional field to give a coherent and comprehensive understanding of a specific thing. Even though humans can comprehend things that are out of context and not in chronological order, the computer must have all the information available and placed in a structured way to execute a program correctly. An example on how to simplify something using flowcharts is how UBERMORGEN (Alessandro Ludovico and Paolo Cirio) uses flowcharts to illustrate their work “Google will eat itself” (2005) and “The Project Formerly Known as Kindle Forkbomb Printing Press” (2013).