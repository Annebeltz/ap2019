# AP2019

**MINI_EX6**

**Link:**
https://gl.githack.com/Annebeltz/ap2019/raw/master/Mini_ex6/index.html

![Screenshot](Skærmbillede_mini_ex6.png)


My game is very simple. I got a lot of inspiration from the famous game, Flappy Bird. Although it has some of the same features, it still takes some distance from the original game. You play the game by pressing the mouse button. In order to avoid the spider webs, you sometimes have to hold the mouse down. My goal was to make some kind of game over when you either touched the spider webs or flew over or under the canvas. My coding skills did not reach this goal, unfortunately. Because of this missing feature, you can play the game into infinity. That was not my intention. 

My program has two classes: the fly and the spider webs. There is only one object in the fly class but a variety of objects in the spider class. I used the syntax frameCount to randomly make spider webs appear each 100th time the canvas is looped. I called the speed and the minimum amount of spider web objects in order to control the spider webs so the program did not spit a 100 objects at a time.
The other object, the fly, had a lot of different syntaxes and functions. I made a function mousePressed(), which makes the fly go up each time you press the mouse/or hold it down. In order to make the fly go down when the mouse is not pressed, I had to call the syntax gravity and velocity. At first, I did not work, but then I tried playing around with the number and then I eventually cracked the code. I had a hard time making the fly work and it made me very frustrated when I could not make it work even though I tried for 7 hours. But the "Shut up and code"-session helped me a lot when Ester instructed me through the errors. 

I think the text by Fuller and Goffey was very abstract and confusing at first. But after watching Daniel Shiffman's videos I made a lot more sense. An object is like a specific instance: "objects are instances of classes" (Lee 2013, p. 20). From Shiffman's perspective creating an object is like a template or blueprint. Object-oriented programming to me can be described in one question: "what does it mean to be an object". In my case what does it mean to be a fly? Which characteristics does a fly have? In my program, the fly could move(), show() and in between had an x-axis, y-axis and a position in the canvas. All the features in the class are something that is what the object is defined as. The class consists of both data and functionality. I believe that a class always should consist of a constructor (and move(), show()). The constructor can be compared to the function setup() which define what the object should consist.
Defining object-oriented programming in Fuller and Goffey's point of view: "Objects, in object orientation, are groupings of data and the methods that can be executed on that data, or stateful abstractions" (Fuller & Goeffey 2017, p.1).  The development of object-oriented programming is a new aspect of the interaction between the user and the machine. The computer has a broader impact on our conceptual thoughts today than they had just 30 years ago.