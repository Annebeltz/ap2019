var words = ["Loading...", "Processing...", "Unpacking...", "Charging...", "Storing...", "Filling...", "Prepares...", "Converts...", "Transforming..."];
var index = 0;

function setup() {
 createCanvas(windowWidth, windowHeight);
 background(10);
 frameRate (10);

}

function draw() {
  fill(200, 180, 200)
  rect(0, 0, width, height);
  drawThrobber(10);
}

function drawThrobber(num) {
  push();
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255);
  quad(35,0,22,22,150,20,80,50);
  fill('rgba(255, 170, 150, 0.9)');
  quad(35,0,22,22,200,50,20,30);
  fill('rgb(180,250,200)');
  quad(35,0,22,22,150,70,10,10);
  fill(255, 240, 130);
  quad(35,0,22,20,150,100,25,40);
  fill(255, 240, 130);
  quad(100,0,22,20,150,100,25,40);
  fill('rgb(180,250,200)');
  quad(100,30,10,20,150,100,25,40);
  fill('rgba(255, 170, 150, 0.9)');
  quad(35,0,22,22,200,50,20,30);
  fill('rgba(150,255,150, 0.25)');
  quad(100,0,22,22,150,20,80,50);
  fill(255);
  quad(20,10,50,50,150,20,80,50);
  fill('rgba(255, 170, 150, 0.9)');
  quad(100,10,10,10,150,30,35,30);
  fill(255, 240, 130);
  quad(100,0,22,22,150,30,50,10);
  fill(255);
  quad(20,0,20,20,100,20,20,20);
  pop();

textStyle(ITALIC);
fill(255);
textSize(50);
text(words[index], 610, 65);

textStyle(NORMAL);
fill(255);
textSize(20);
text("**press the mouse**", 620, 680);

}

function mousePressed() {
  index = index + 1;
if(index == 8) {
  index = 0;
}
}
