function setup() {
createCanvas(windowWidth, windowHeight);
background(200);

}

function draw() {
//baggrund
fill('#fae');
//omkreds af emoji
ellipse(700, 350, 400, 400);

//øjne emoji_1
fill(255);
ellipse(610, 300, 50, 50);
ellipse(780, 300, 50, 50);
fill(125, 85, 10);
ellipse(610, 308, 30, 30);
ellipse(780, 308, 30, 30);
fill(50);
ellipse(610, 311, 19, 19);
ellipse(780, 311, 19, 19);
fill(255);
arc(690, 400, 150, 150, 0, PI);


//skæg
rect(690, 380, 1, 8);
rect(700, 382, 1, 8);
rect(710, 380, 1, 8);
rect(720, 382, 1, 8);
rect(730, 380, 1, 8);
rect(740, 382, 1, 8);
rect(750, 380, 1, 8);
rect(760, 381, 1, 8);
rect(770, 380, 1, 8);
rect(680, 382, 1, 8);
rect(670, 380, 1, 8);
rect(660, 381, 1, 8);
rect(650, 380, 1, 8);
rect(640, 382, 1, 8);
rect(630, 380, 1, 8);
rect(620, 381, 1, 8);
rect(610, 381, 1, 8);
rect(635, 378, 1, 5);
rect(645, 378, 1, 5);
rect(655, 378, 1, 5);
rect(665, 378, 1, 5);
rect(675, 378, 1, 5);
rect(685, 378, 1, 5);
rect(695, 378, 1, 5);
rect(705, 378, 1, 5);
rect(715, 378, 1, 5);
rect(725, 378, 1, 5);
rect(735, 378, 1, 5);
rect(745, 378, 1, 5);
rect(755, 378, 1, 5);
rect(635, 378, 1, 5);
rect(625, 378, 1, 5);

// emoji_2
fill(255);
ellipse(200, 350, 400, 400);
fill(100, 230, 255);
ellipse(200, 350, 300, 300);
fill(0);
ellipse(200, 350, 150, 150)

//kamera
fill(255);
rect(230, 250, 80, 80, 8, 8, 8)
fill(100);
rect(245, 265, 50, 50, 8, 8, 8)
fill(0);
ellipse(270, 290, 30, 30);
fill(255);
ellipse(270, 290, 15, 15);


}
