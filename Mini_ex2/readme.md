![ScreenShot](mini_ex2_skærmbillede1.png)

![ScreenShot](mini_ex2_skærmbillede2.png)

![ScreenShot](mini_ex2_skærmbillede3.png)

Link:
https://gl.githack.com/Annebeltz/ap2019/raw/master/Mini_ex2/index.html



**• Describe your program and what you have used and learned**
My program is two emojis. One representing an eye with an old school inspired camera. The other emoji is a pink smiley with a beard. The program was simple to make but took a lot of time because of the details behind the beard. I had some frustration behind the eye emoji because I wanted to copy the Instagram camera, but did not have the skills to do it. I was in Austria and therefore I could no get help from my co-students. 

**• What have you learned from reading the assigned reading? How would you put your emoji into a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend some time in thinking about it)**
Reading my text made me realize that emojis is not just a different way to "style" your messages. It represents political as well as social identities and perspectives. The text inspired me to make an aesthetic statement which involves the new sexualities in society. We move slowly towards the acceptance of different sexualities and genders, but some still live in the world of a patriarchal mind. To contradict this mind I made a pink smiley with a beard that represents the feminine and masculine worlds coming together as a whole and a new perspective.
The eye emoji represents the downside of social media takes over. The reason why I made this one is because of the power that we give social media like Instagram. They have the power over the way we want to show our best to the surroundings. There is no time for depressed moments or just a bad day. You have to be your "true" and best self all the time. I made the eye the blue color because it symbolizes the naive and almost hypnotized mind. 
