//variables
let font;
let abstraction;
let points;
let sway;
let continuity = 0;

//Load JSON and font
function preload() {
  font = loadFont('font.otf');
  abstraction = loadJSON('nothingness.json');
}


//setup and swaying headline: "What is this?"
function setup() {
  createCanvas(windowWidth, windowHeight);
  background(100, 204, 200);

  stroke(255);
  fill(255, 150, 200);

  points = font.textToPoints(' WHAT IS THIS? ', 8, -1, 7, {
    sampleFactor: 10,
    simplifyThreshold: 0
  });
    sway = font.textBounds(' WHAT IS THIS? ', -1, 0, 10);
}

//Nothingness including textsize and loop
function draw() {
  frameRate(10);

  let suggestion = abstraction.nothingness;
    textSize(24);

  if(frameCount%10==0===true){
    continuity = continuity+1
  }

  if(frameCount%50==0===true){
    fill(100, 204, 200);
    rect(0,0,windowWidth, windowHeight);
  }

//random text position
  text(suggestion[continuity], random(0, 1200), random(0, 1200));

// rectangle covering the swaying headline
push();
noStroke();
fill(100, 204, 200);
rect(200, 130, 1000, 500);
pop();

//The sway effect
beginShape();
  translate(-sway.x * width / sway.w, -sway.y * height / sway.h);
  for (let i = 0; i < points.length; i++) {
    let p = points[i];
        vertex(
      p.x * width / sway.w +
        sin(10 * p.y / sway.h + millis() / 300) * width / 100,
      p.y * height / sway.h
    );
  }
endShape(CLOSE);
}
