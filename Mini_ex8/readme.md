**Link:** https://gl.githack.com/Annebeltz/ap2019/raw/master/Mini_ex8/index.html

![Screeenshot](Screenshot8.png)

I was collaborating with Freya Wattez for this mini_ex.
Firstly, Freya and I sat down and talked about which idea we wanted to execute into a program by code. We discussed several ideas but ended on the idea regarding a question and some potential answers to this question. In order to implement electronic literature that utilizes text as the main medium, we decided to make the headline/question in a sway mode by using the syntax Shape and creating a for() loop to make it move. By calling different points in setup and inside the for loop makes the headline graphical and the contrast between the two distinctive and eye-catching colors. The suggestions which are represented by different words like "queer?", "art?", "sentence?" etc from the JSON file are randomly executed in the canvas both by position and word.  

Freya and I took an alternative perspective on vocable code and language in software. We got some inspiration from attending the symposium, where the talk by Geoff Cox caught our attention which we want to acknowledge. Cox talked about the book "Ways of seeing" by John Berger where Cox pointed out different keywords from the book. The fact that seeing comes before words regarding the picture of a pipe which in fact is not a pipe, but a picture of one. As John Berger quotes "perspective makes the eye the center of the visible world". In relation to our program, we wanted to rise and promote the same frustration of what "WHAT IS THIS?". In one perspective it could be some sort of art, and in another perspective, it could be just a sentence. Some may recognize the colors by first sight. It is situated and that is the main goal of our program. There is much more to it than just a sentence and some random words, but it is up to the audience/user to find out what it is. There is no wrong answer. That it can be interpreted in a lot of different ways, and the JSON file with the different suggestions could be a hundred miles. 

The aesthetic aspect of the relationship between code and language is the variables we named. As an example, we called the JSON file "nothingness" and referred it to a variable called "suggestions" because the program is nothing and everything, it is how the user/audience interpret with the program. The random words are only suggestions to what it might be and that is also why we intentionally implemented a question mark behind each suggestion. The program and the intention with the program are indeed abstract and thereby very situated and fluffy. The program is just another way to interpreting language in software just like the different ways mentioned in the text "vocable code". 

So what is your view on the program? WHAT IS THIS? What is it?




