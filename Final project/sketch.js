let angle = 0;
let img;
let img1;


function preload (){
  img = loadImage('satellite.png');
  img1 = loadImage('space3.png');
}

function setup(){
createCanvas(windowWidth, windowHeight);
angleMode(DEGREES);
frameRate(100);
textSize(60);
textAlign(CENTER);
}

function draw(){
image(img1, 0, 0, 1500, 1000);

Earth();
death();
satellite();

}


function death(){
  if (frameCount<750){
    ellipse(780,240,30,10);
    ellipse(665,255, 120, 20);
    ellipse(685,240, 100, 35);
    ellipse(640,247,30,30);
  }

  if (frameCount<1500){
    ellipse(630,252,30,30);
    ellipse(710,230, 100, 30);
    ellipse(640,270, 20, 80);
  }

  if (frameCount<2250){
    ellipse(620,305, 80, 20);
    ellipse(572,350, 80, 90);
    ellipse(600,370, 100, 80);
    ellipse(665,370, 40, 80);
  }

  if (frameCount<3000){
    ellipse(600,400, 80, 150);
    ellipse(630,420, 80, 150);
    ellipse(665,400, 50, 150);
  }

  if (frameCount<3750){
    ellipse(605,495, 20, 150);
    ellipse(690,350, 60, 60);
    ellipse(690,330, 30, 60);
  }

  if (frameCount<4500){
    ellipse(650,550, 50, 70);
    ellipse(632,560, 50, 70);
    ellipse(610,568, 20, 30);
  }

  if (frameCount<5250){
    ellipse(680,555, 80, 60);
    ellipse(680,575, 110, 70);
    ellipse(660,567, 110, 70);
  }


  if (frameCount<6000){
  ellipse(710,582, 100, 70);
  ellipse(870,400, 60, 100);
  ellipse(897,400, 50, 80);
  }

  if (frameCount<6750){
    ellipse(855,410, 30, 80);
    ellipse(897,450, 40, 80);
    ellipse(850,410, 40, 80);
  }

  if (frameCount<7500){
  ellipse(900,480, 20, 30);
  ellipse(865,325, 20, 40);
  ellipse(874,315, 40, 30);
  }

  if (frameCount<8250){
  ellipse(898,485, 20, 30);
  ellipse(902,420, 40, 110);
  ellipse(680,500, 50, 10);
  }

  if (frameCount<9000){
    ellipse(700,580, 110, 70);
    ellipse(620,555, 60, 60);
    ellipse(700,340, 80, 50);
  }
}

function Earth(){
  //The Earth
    noStroke();
    fill(20,75,200); //the colour of the globe
    circle(720,415,202);
    //the "mainland"
    fill('hsb(160, 100%, 50%)');//the colour of "mainland" on the globe
}

function satellite(){
  translate(720,415);
  rotate(angle);
  fill(255);
  rectMode(CENTER);
  scale(0.1);
  image(img,1500,1500);

  angle = angle + 0.5
}
