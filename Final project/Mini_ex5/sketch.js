
let insta;
let button;
let beard;
let wig;
let eyes;

function setup(){
createCanvas(windowWidth, windowHeight);
background(240);

//INFO TEXT
textSize(20);
text('Press the instagram icon', 350, 600);

textSize(20);
text('Press space key', 930, 600);

//EYE
insta=createImg('insta.png');
insta.position(460, 300);
insta.size(35, 35);

button=createButton('O');
button.position(466, 309);
button.style('font-size', '10px');
button.style('background-color', 'rgba(100%,100%,100%,0.1)');
button.mousePressed(change);

beard=createImg('beard.png');
beard.position(925, 330);
beard.size(150, 150);

//EMOJI
wig=createImg('wig2.png');
wig.position(640, 50);
wig.size(700, 700);

eyes=createImg('eyes.png');
eyes.position(920, 270);
eyes.size(170, 70);


}
//EYE - text
function change(){
  button.style('background-color', 'rgba(100%,100%,100%,0.1)');
  textSize(50);
  fill("#fae");
  text('We are all addicted', 230, 200);

}

function draw(){
//EYE
  textSize(300);
  text('👁️‍🗨️', 300, 500);
  fill(120, 200, 255);
  noStroke();
  ellipse(450, 345, 110, 110);
  fill(30);
  ellipse(450, 345, 60, 60);

//SKIN EMOJI
  if (keyIsPressed) {
    r=random(255, 50, 0);
    g=random(255, 50, 0);
    b=random(255, 50, 0);
      fill(r, g, b);
      ellipse(1000, 350, 300, 300);

  }

}
