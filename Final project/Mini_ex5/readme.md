**Link:** https://gl.githack.com/Annebeltz/ap2019/raw/master/Mini_ex5/index.html

![Screenshot](mini_ex2_skærmbillede1.png)

![Sreenshot](Skærmbillede_mini_ex5.png)


In my mini_ex for this week, I upgraded my emojis in the sense that they now have more details and is interactive. When I made the emojis last time I did not have a lot of time because of some personal reasons. My feedback was therefore related to the unfinished and slightly simple program. I went with the same basic details a changed it up a bit to make it stand out even more than they did before. In order to do this, I used the DOM library to connect a button to the eye that I made. When the button has pressed a text saying "we are all addicted" appears. I made my own function(change) to tell the program that something is changing when the button is pressed. In both of the emojis, I used the text() syntax and the createImg(), to use realistic pictures (take a look at the two screenshots). I changed the hair, and eyes by using the createImg() syntax, to make the emoji more fun and realistic. Although these elements look more realistic I still wanted to keep the round and original shape of an emoji. An if() statement is also implemented in the right emoji, when the keyboard is pressed. When a random key is pressed the ellipse/the face of the emoji changes into random colors. 
Furthermore, two instructional texts are implemented down below the two emojis in order for the user to be interactive with my program. 

The concept for my emoji is to look at something old (in this case my mini_ex2) and change it up and into a new context but still preserve the thoughts and concept of the old project. Like many other websites and companies, they change and upgrade the design of the logo, website etc. But they still keep the trademark and the details that represent their brand. This was what I tried to complete in this program and mini_ex.

Concerning the concept of the program as mentioned in my mini_ex2 the text "Modifying the universal" by Abbing, R.R, Pierrot, P, and Snelting F made me realize that emojis is not just a different way to "style" your messages. The emojis have a tendency to represent the political as well as social identities and perspectives in our lives. The text inspired me to make an aesthetic statement which involves the new sexualities in society. We move slowly towards the acceptance of different sexualities and genders, but some still live in the world of a patriarchal mind. To contradict this mind I made the right smiley with a mustache and long pink hair that represents the feminine and masculine worlds coming together as a whole and a new perspective. The reason why it changes colors is to highlight that anyone is allowed to be who they are no matter your ethnicity, social or economic background.

The eye emoji represents the downside of social media takes over. The reason why I made this one is because of the power that we give social media like Instagram. They have power over the way we want to show our best to the surroundings. There is no time for depressed moments or just a bad day. You have to be your "true" and best self all the time. I made the eye the blue color because it symbolizes the naive and almost hypnotized mind. The text that appears when pushing the Instagram button is to highlight that we all are addicted to the feed from social media, especially Instagram. 


Based on the text "Coding Literacy. Coding for everyone and the legacy of mass literacy" by Annette Vee she presents four main dominant arguments for learning to code which includes:
1/ individual empowerment
2/ learning new ways to think
3/ citizenship and collective progress
4/ employability and economic concerns (p. 45) 
When reading the text I agreed on most of the statements. I think it is very important to have individual empowerment to the extent that you have the confidence and strength to set and complete realistic goals. Furthermore, it is important that you understand different syntaxes and have the ability to put them together and see it in a complete perspective.
Although I agree on the first two arguments, I think that in the future, the ability to programming and coding becomes a literacy too - like reading and writing. We live in a digital culture and the development in the digital culture only increases. 
Programming becoming a literacy can have some consequences we need to be aware of when pushing coding as a literacy.
We have to be aware that we do not neglect the basics and fundamental concepts of literacy. We still believe that we need labor in other professions than just the technological and digital subjects and work areas. If we seek too much on getting programming to be literacy, we may forget all the other aspects not concerning technology. These aspects and professions are important too. 


