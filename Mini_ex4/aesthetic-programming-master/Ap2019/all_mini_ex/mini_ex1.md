# Weekly mini ex1: due week 7, Monday night - Think about My First Program

**Objective:**
- To learn the basic setup, including independent study of code syntax, uploading files on gitlab, creating a readme file, etc.
- To get familiar with the building and feedback process of weekly mini exercises
- To reflect upon the process of coding

**Get some additional inspiration here:**
- [Daily sketch in Processing](https://twitter.com/sasj_nl) and her talk is [here](https://www.youtube.com/watch?v=nBtGpEZ0-EQ&fbclid=IwAR119xLXt4nNiqpimIMWBlFHz9gJNdJyUgNwreRhIbdJMPPVx6tq7krd0ww) + [All the Daily Things 2018](https://vimeo.com/309138645) by Saskia Freeke
- [zach lieberman](https://www.instagram.com/zach.lieberman/)
- [Basics in OpenProcessing](https://www.openprocessing.org/browse/?q=basics&time=anytime&type=all#)
- [Creative Coding with Processing and P5.JS](https://www.facebook.com/groups/creativecodingp5/)

**Tasks (RUNME and README):**
1. Make sure you have read/watch the required readings/instructional videos and references
2. Study at least one syntax from the list of p5.js' [reference](https://p5js.org/reference/) (of course, it is always good to know more different syntax. Be curious!)
3. Familiar yourself with the reference structure: example, description, syntax and parameters (This becomes your essential and life-long skills for learning new syntax on your own)
4. Use, Read, Modify (or even combine) the sample code that you found (the most basic level is changing the numbers), and produce a new sketch as a 'runme'
5. Upload your 'runme' to your own Gitlab account under a folder called **mini_ex1**. (Make sure your program can be run on a web browser)
6. Create a readme file (README.md) and upload to the same mini_ex1 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:
  - A screenshot about your program (search for how to upload a screenshot image and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/
  - Describe your first independent coding process (in relation to thinking, reading, copying, modifying, writing, uploading, sharing, commenting code)
  - How your coding process is differ or similar to reading and writing text? (You may also reflect upon Annette Vee's text on coding literacy)
  - What is code and coding/programming practice means to you?

7. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)"  (Feedback is due before next Wed tutorial class while the readme and runme are due on next Mon)

NB1: Feel Free to explore and experiment more syntax

NB2: The readme file should be within 5600 characters.

**First mini exercise peer-feedback: guideline**
1. Describe what is the program about and what syntaxes were used
2. Reflect upon what are the differences between reading other people code and writing your own code. What can you learn from reading other works?
